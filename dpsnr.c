// calculate DPSNR metrics of 2 input files and output result score to stdout

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#define STBI_NO_LINEAR
#define STBI_NO_HDR
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


int clamp(int val, int min, int max){
    if (val < min) val = min;
    if (val > max) val = max;
    return val;
}

int at(int i, int j, int k, int w, int h, int n){
    i = clamp(i, 0, w);
    j = clamp(j, 0, h);
    return j*w*n+i*n+k;
}


// box blur (actually gaussian) with range=1, occurs in-place
void box(unsigned char *bmp, int w, int h, int n)
{
    int i,j,k;
    int c;
    unsigned s=0;
    int first, second, third;
    
    if (n>3) c=3;
    else c=n;
    
    // horizontal blur:
    for (k=0; k<c; k++){
	for (j=0; j<h; j++){
	    first = bmp[at(-1,j,k,w,h,n)];
	    second = bmp[at(0,j,k,w,h,n)];
	    third = bmp[at(1,j,k,w,h,n)];
	    s = first+second*2+third;
	    for (i=0; i<w; i++){
		bmp[at(i,j,k,w,h,n)]= s/4;
		s-=first;
		s-=second;
		s+=third;
		first = second;
		second = third;
		third = bmp[at(i+2,j,k,w,h,n)];
		s+=third;
	    }
	}
    }
    
    // vertical blur:
    for (k=0; k<c; k++){
	for (i=0; i<w; i++){
	    first = bmp[at(i,-1,k,w,h,n)];
	    second = bmp[at(i,0,k,w,h,n)];
	    third = bmp[at(i,1,k,w,h,n)];
	    s = first+second*2+third;
	    for (j=0; j<h; j++){
		bmp[at(i,j,k,w,h,n)]= s/4;
		s-=first;
		s-=second;
		s+=third;
		first = second;
		second = third;
		third = bmp[at(i,j+2,k,w,h,n)];
		s+=third;
	    }
	}
    }
}
 

double PSNR(unsigned char *img1, unsigned char *img2, uint32_t width, uint32_t height, uint32_t channels)
{  
    double MSE=0.0;
    double psnr=0.0;
    uint32_t index = 0;
    uint32_t i=0;
    uint32_t echannels;
    double temp;

    if (channels > 3) echannels = 3;
    else echannels = channels;  
 
    for(index=0;index<width*height;index++)
    {
    	for (i=0; i<echannels; i++){
    		temp = img1[channels*index+i]-img2[channels*index+i];
        	MSE += temp*temp;
        }
    }
    MSE/=width*height;
    MSE/=echannels;

    //avoid division by zero
    if(MSE==0) return 99.0;
    psnr=10*log10(255*255/MSE);
 
    return psnr;
}

int main(int argc, char **argv)
{
	int h1, w1;
	int h2, w2;
	int n1, n2;
	unsigned char *data1;
	unsigned char *data2;
	int i;
	double psnr;
	
	if (argc != 3){
		printf("Usage: %s {file1} {file2}\n", argv[0]);
		return -1;
	}
	
	data1 = stbi_load(argv[1], &w1, &h1, &n1, 0);
	data2 = stbi_load(argv[2], &w2, &h2, &n2, 0);
	
	if (data1 == NULL){
		printf ("Unable to load image %s\n", argv[1]);
		return 1;
	}
	if (data2 == NULL){
		printf ("Unable to load image %s\n", argv[2]);
		return 2;
	}
	if ((w1!=w2)||(h1!=h2)||(n1!=n2)){
		printf ("Images must have same dimensions (width, height) and number of channels: %s (%dx%d, %d), %s (%dx%d, %d)\n", argv[1], w1, h1, n1, argv[2], w2, h2, n2); 
		return 3;
	}
	
	psnr = PSNR(data1,data2,w1,h1,n1);
	//printf("psnr = %f\n", psnr);
	
	for (i=0; i<4; i++){
	    box(data1, w1, h1, n1);
	    box(data2, w2, h2, n2);
	}
	
	printf("%f\n", sqrt(psnr*PSNR(data1, data2, w1, h1, n1))*2);
	stbi_image_free(data1);
	stbi_image_free(data2);
	return 0;
}

