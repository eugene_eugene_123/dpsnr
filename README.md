Proposal of new metrics for objective evaluation of image quality (presentation can be found in "Downloads" folder).
Simple command line utilities dpsnr.pl (requires Perl and ImageMagick) or compiled dpsnr.c (no additional requirements, only stb_image.h header file) can be used to evaluate these metrics.
Results of dpsnr.pl and dpsnr.c can be slightly different due to different blur algorithms used in dpsnr.pl and dpsnr.c.

"Downloads" folder contains programs (fast.exe and slow.exe) for generation of compressed etc1 textures using algorithm based on proposed DPSNR metrics. Programs etc2fast.exe and etc2slow.exe compress into subset of etc2 format (etc1+gradient blocks)

TODO: add Sobel filter to further improve quality of metrics