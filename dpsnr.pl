#! /usr/bin/perl
use warnings;

die "Usage: $0 {file1} {file2}\n" unless $ARGV[1];
die "Unable to find $ARGV[0]\n" unless (-e $ARGV[0]);
die "Unable to find $ARGV[1]\n" unless (-e $ARGV[1]);

$z = $ARGV[0];
$t = $ARGV[1];

$x = "_".$ARGV[0];
$y = "_".$ARGV[1];

`magick convert -blur 0x4 $z $x\n`;
`magick convert -blur 0x4 $t $y\n`;
`magick compare $z $t -metric PSNR diff.bmp 2> temp1.txt\n`;
`magick compare $x $y -metric PSNR diff.bmp 2> temp2.txt\n`;

open IN1, "temp1.txt" or die $!;
open IN2, "temp2.txt" or die $!;

while (<IN1>){
    chomp;
    $q1 = $_;
}

while(<IN2>){
    chomp;
    $q2 = $_;
}

close IN1;
close IN2;

unlink("diff.bmp");
unlink("temp1.txt");
unlink("temp2.txt");
unlink($x);
unlink($y);

$result = sqrt($q1*$q2)*2;
print "$result\n";

